""" Implement the update stage in the Dune CI Workflow

The update stage does the following steps:
* Clone all needed modules if not already present
* Determine a branch to use for modules
* Switch to that branch if it exists
"""

import functools
import git
import os
import re
try:
    import urlparse
except ImportError:
    import urllib.parse as urlparse

from dune.citools.modulefile import parse_dune_module_file
from dune.citools.config import get_argument, get_duneci_option
from dune.citools.repositories import get_dune_repo_url
from dune.citools.error import DuneCIGitError


@functools.total_ordering
class ForkPointComparator(object):

    def __init__(self, repo, fp):
        self.repo = repo
        self.fp = fp

    def __gt__(self, other):
        return self.repo.is_ancestor(self.fp, other.fp)


def sorted_fork_points(repo, fork_points):
    for fpc in sorted(ForkPointComparator(repo, fp) for fp in fork_points):
        yield fpc.fp


def associate_branch_with_fork_point(repo, fp):
    # Find all the branches that have the fork point as an ancestor
    branches = [b.name for b in repo.branches if repo.is_ancestor(fp, b)]

    # If result is unique, we return it
    if len(branches) == 1:
        return branches[0]

    # Implement the heuristic written down in README.md
    if "master" in branches:
        return "master"

    if any(b.startswith('releases/') for b in branches):
        for b in sorted(branches, reverse=True):
            if b.startswith('releases/'):
                return b

    return branches[0]


def dune_citools_update():
    # Parse the dune module file of the module that we are testing
    module = parse_dune_module_file("./dune.module")

    # Gather branch information
    repo = git.Repo('.')

    # Initialize heads from remote branches!
    for ref in repo.refs:
        if isinstance(ref, git.RemoteReference):
            branch = re.match("origin/(.*)", ref.name)
            if branch:
                branch = branch.groups(1)[0]
                if branch != 'HEAD' and branch not in [b.name for b in repo.branches]:
                    git.Head.create(repo, branch, reference=ref)

    # Determine current branch through GitLab CI's environment variables
    current_branch = os.environ.get("CI_BUILD_REF_NAME", [])
    if current_branch:
        current_branch = [current_branch]

    # First step: Find fork points with other heads
    fork_points = []
    for head in repo.heads:
        if repo.head.is_detached or repo.head != repo.active_branch:
            fork_points.extend(repo.merge_base(repo.head, head))
    # Discard duplicates
    fork_points = frozenset(fork_points)

    # Second step: Sort the fork points by increasing distance to our head
    merge_branches = [associate_branch_with_fork_point(repo, fp) for fp in sorted_fork_points(repo, fork_points)]

    # Collect a full priority list of branches to checkout in downstream modules
    branches = current_branch + merge_branches + ["master"]

    # Prepare all the modules. That includes:
    # * Cloning it if not present
    # * Selecting the correct branch
    prepare_requirements(module, branches=branches)


def prepare_requirements(modfile, branches=(), forced_urls={}, forced_branches={}, configpath='.'):
    # Read the manual specification from this modules
    manual_specs = get_duneci_option('upstream', default={}, path=configpath)
    for module in manual_specs:
        if "url" in manual_specs[module]:
            forced_urls.setdefault(module, manual_specs[module]["url"])
        if "branch" in manual_specs[module]:
            forced_branches.setdefault(module, manual_specs[module]["branch"])

    for requ in modfile.depends:
        prepare_dune_module(requ,
                            branches=branches,
                            forced_urls=forced_urls,
                            forced_branches=forced_branches,
                            configpath=configpath
                            )

        # Now process requirements recursively
        modfilepath = os.path.join(get_argument('sources_path'), requ, 'dune.module')
        modfile = parse_dune_module_file(modfilepath)
        prepare_requirements(modfile,
                             branches=branches,
                             forced_urls=forced_urls,
                             forced_branches=forced_branches,
                             configpath=os.path.join(get_argument('sources_path'), requ)
                             )

    # Check that all requirements are present (some may have been delayed
    # due to inaccessibility of an URL
    for requ in modfile.depends:
        if not os.path.exists(os.path.join(get_argument('sources_path'), requ)):
            raise DuneCIGitError("Unable to locate a remote repository for Dune module {}".format(requ))

    # TODO Think about handling suggestions here.


def switch_branch_if_existent(modname, repo, remote, branch):
    for ref in repo.refs:
        if ref.name == remote.name + '/' + branch:
            ref = git.Head.create(repo, branch, reference=ref)
            ref.checkout()
            print("Checked out branch for module {}: {}".format(modname, branch))
            return ref
    return None


def prepare_dune_module(name, branches=(), forced_urls={}, forced_branches={}, configpath=None):
    assert configpath

    # Extract the source directory from configuration:
    sourcedir = os.path.join(get_argument('sources_path'), name)

    # Check whether this URL was previously manually specified
    url = forced_urls.get(name, None)

    # Check whether we got a hint where to take this repository from
    if not url:
        url = get_duneci_option('upstream', default={}, path=configpath).get(name, {}).get('url', None)

    # Check standard locations next
    if not url:
        url = get_dune_repo_url(name)

    # If we do not have an URL by now, we need to wait, some other downstream module
    # might hard-code a dependency.
    if not url:
        return

    # Implement token authentication, if necessary
    token = get_duneci_option('upstream', default={}, path=configpath).get(name, {}).get('token', None)
    if token:
        url = urlparse.urlparse(url)
        url = url._replace(netloc='gitlab-ci-token:{}@{}'.format(token, url.netloc))
        url = urlparse.urlunparse(url)

    # If the source directory doesn't exist, we clone it
    try:
        repo = git.Repo(sourcedir)
    except git.NoSuchPathError:
        repo = git.Repo.clone_from(url, sourcedir)
        print("Cloned dependency {} to {}:".format(name, sourcedir))
        print("Used remote for {}: {}".format(name, url))

    # Check that our repository has the correct remote
    correct_remote = None
    for remote in repo.remotes:
        for remote_url in remote.urls:
            if str(remote_url) == url:
                correct_remote = remote

    if not correct_remote:
        correct_remote = repo.create_remote("ciremote", url)
        print("Switched remote for {}: {}".format(name, url))

    # Fetch latest changes
    correct_remote.update()

    manual_branch = None

    # Check whether a branch name was previously forced
    if name in forced_branches:
        manual_branch = forced_branches[name]

    ref = None

    # First try to switch to a branch that has explicitly been set in dune-ci.yml
    if manual_branch:
        ref = switch_branch_if_existent(name, repo, correct_remote, manual_branch)
        if not ref:
            raise DuneCIGitError("The manually specified branch {} of module {} does not exist".format(manual_branch, name))

    # Now try to switch to those branches that we gathered from inspecting the tested module
    # The last of these is master, which should always exist
    if not ref:
        for branch in branches:
            if not ref:
                ref = switch_branch_if_existent(name, repo, correct_remote, branch)

    # Now that we are on the correct branch, pull!
    correct_remote.pull(refspec=ref)
