""" Implementation of the CI stages that can be handled through a dunecontrol call """

import functools
import os

from dune.citools.logging.dunecontrol import DuneControlLogSplitter
from dune.citools.config import get_argument
from dune.citools.logging.context import log_dir


def dunecontrol_stage(tag, args):
    """ A stage, that uses dunecontrol

    tag is to be used to initialize the DuneControlLogSplitter
    instance in use. args are arguments to dunecontrol.
    """
    srcdir = get_argument("sources_path")
    currentdir = os.path.abspath('.')

    # Determine location of dune-common
    commonpath = os.path.abspath(os.path.join(srcdir, 'dune-common'))
    if not os.path.exists(commonpath):
        commonpath = currentdir

    # Check for existence of the dunecontrol script
    dunecontrol = os.path.join(commonpath, 'bin', 'dunecontrol')
    if not os.path.exists(dunecontrol):
        raise FileNotFoundError("cannot determine the location of dunecontrol (was looking at {})".format(dunecontrol))

    # Stringify the given arguments
    if not isinstance(args, str):
        args = " ".join(args)

    # Add a build directory, if none is specified so far
    if "--builddir" not in args:
        args = "--builddir={} {}".format(get_argument("build_path"), args)

    # Construct the command and its environment
    command = "{} {}".format(dunecontrol, args)
    env = dict(os.environ)
    env['DUNE_CONTROL_PATH'] = "{}:{}".format(srcdir, currentdir)

    with DuneControlLogSplitter(tag) as logsplitter, log_dir('./logs'):
        logsplitter.log_from_subprocess(command, env=env)


_build = functools.partial(dunecontrol_stage, "make", "make")
_buildtests = functools.partial(dunecontrol_stage, "build_tests", "--current make build_tests")
_cmake = functools.partial(dunecontrol_stage, "cmake", "configure")
_ctest = functools.partial(dunecontrol_stage, "ctest", "--current bexec ctest")
