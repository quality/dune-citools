"""
A log splitter for dunecontrol calls.

It writes a separate log for each module, as well as a joint one.
"""
import re

from dune.citools.logging.splitter import LogSplitterBase
from dune.citools.logging.writer import StdoutLogWriter, FileLogWriter


class DuneControlLogSplitter(LogSplitterBase):

    def __init__(self, prefix):
        LogSplitterBase.__init__(self)
        self.prefix = prefix

        self.register_log(StdoutLogWriter())

    def parseString(self, line):
        # Decode line
        line = line.decode()

        # Check whether a new Dune module is treated here
        newmodline = re.match('--- calling (.+) for (.+) ---', line)
        if newmodline:
            module = newmodline.groups()[1]
            self.register_log(FileLogWriter("module", "{}-{}.log".format(self.prefix, module)), openstream=True)

        if "module" in self.tag_to_writer:
            yield ("module", "stdout"), line
        else:
            yield ("stdout",), line

        if re.match('--- (.+) done ---', line) or re.match('--- Failed to build (.+) ---', line):
            self.unregister_log("module")
