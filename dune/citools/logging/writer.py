import os
import sys

from dune.citools.logging.context import get_log_dir


class LogWriterBase(object):
    """ Base class for an object that writes log to a resource

    Each writer is associated a tag. These tags are used by log splitters
    to request a writer to write a specific line. LogwriterBase instances
    need to be registered with a log splitter in order to be operational.
    """
    def __init__(self, tag, output_stream=None):
        self.tag = tag
        self.output_stream = output_stream

    def writeLine(self, line):
        self.output_stream.write(line)

    def openStream(self):
        """ A hook to initialize a (stream) ressource """
        pass

    def closeStream(self):
        """ A hook to deinitialize a (stream) ressource """
        pass


class FileLogWriter(LogWriterBase):
    """ A writer class for file-based logging """

    def __init__(self, tag, filename):
        path, filename = os.path.split(filename)
        assert(not os.path.isabs(path))
        path = os.path.join(get_log_dir(), path)
        if not os.path.exists(path):
            os.makedirs(path)
        self.filename = os.path.join(path, filename)

        LogWriterBase.__init__(self, tag)

    def openStream(self):
        self.output_stream = open(self.filename, 'w')

    def closeStream(self):
        self.output_stream.close()


class StdoutLogWriter(LogWriterBase):
    """ A writer for stdout (associated with the tag "stdout" """
    def __init__(self):
        LogWriterBase.__init__(self, "stdout", sys.stdout)


class StderrLogWriter(LogWriterBase):
    """ A writer for stderr (associated with the tag "stderr" """
    def __init__(self):
        LogWriterBase.__init__("stderr", sys.stderr)
