import os

# Storage for the globally stored paths
_log_stack = []


class _LogDirectoryContext(object):

    def __init__(self, directory):
        self.directory = directory
        self.backup = None

    def __enter__(self):
        global _log_stack
        # The first element of the stack is transformed to an absolute path
        if len(_log_stack) == 0:
            if not os.path.isabs(self.directory):
                self.directory = os.path.join(os.getcwd(), self.directory)
        else:
            # If an absolute path is given later, it overrides all of the others
            if os.path.isabs(self.directory):
                from copy import deepcopy
                self.backup = deepcopy(_log_stack)
                _log_stack = []

        # Now append the given path
        _log_stack.append(self.directory)

    def __exit__(self, exc_type, exc_value, traceback):
        global _log_stack
        _log_stack.pop()

        if self.backup:
            from copy import deepcopy
            _log_stack = deepcopy(self.backup)


def log_dir(directory):
    """ A context manager for log directories

    with log_dir("mylog"):
        ...

    Using this context manager sets a log directory,
    which can lateron be retrieved through :ref:`get_log_dir`.
    The context may be used in a nested fashion, which
    will result in appended paths, if relative paths have been given.
    Specifying an absolute path will hide all the previously given
    paths (until the context is left, of course.)
    """
    return _LogDirectoryContext(directory)


def get_log_dir():
    """ Access the logging directory set by :ref:`log_dir`.

    If no context information was given, defaults to the working
    directory.
    """
    global _log_stack
    if len(_log_stack) == 0:
        return os.path.abspath(os.getcwd())
    else:
        return os.path.join(*_log_stack)
