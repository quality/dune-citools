""""
The main log splitting algorithms
"""

import abc
import subprocess


class LogSplitterBase(object):
    """ Base class for any logic consuming a log and distributing it to several log writers

    Inherit from this class and implement the parseString method to get your custom
    log splitter.

    Log splitters are implemented as context managers to ensure safe treatment
    of IO resources:

    with MyLogSplitter() as splitter:
        splitter.log_from_subprocess(...)
        ...
    """

    def __init__(self):
        self.tag_to_writer = {}

    def log_from_generator(self, log_generator):
        """ Process log lines from any python generator """
        for input_line in log_generator:
            for tags, output_line in self.parseString(input_line):
                for tag in tags:
                    self.tag_to_writer[tag].writeLine(output_line)

    def log_from_file(self, filename):
        """ Process log lines from a file on the file system """
        with open(filename, 'r') as f:
            self.log_from_generator(f)

    def log_from_subprocess(self, command, **kwargs):
        """ Process log lines from running a subprocess. Any keyword arguments
        given are passed onto `subprocess.POpen`.
        """
        assert "stdout" not in kwargs
        assert "stderr" not in kwargs
        assert kwargs.pop('shell', True)

        process = subprocess.Popen(command, stdout=subprocess.PIPE, shell=True, **kwargs)
        self.log_from_generator(process.stdout)

    def register_log(self, writer, openstream=False):
        """ Register a log writer instance with this log splitter """
        self.tag_to_writer[writer.tag] = writer
        if openstream:
            writer.openStream()

    def unregister_log(self, tag):
        """ Unregister a log writer instance from this log splitter """
        writer = self.tag_to_writer.pop(tag, None)
        if writer:
            writer.closeStream()

    @abc.abstractmethod
    def parseString(self, line):
        """ The main hook for processing log lines

        Given a line string, this methods should yield tuples (tags, line)
        with tags being a tuple of tags, that correspond to registered log writers
        and line being the (potentially modified) log line.
        """
        return

    def __enter__(self):
        for writer in self.tag_to_writer.values():
            writer.openStream()
        return self

    def __exit__(self, exc_type, exc_value, traceback):
        for writer in self.tag_to_writer.values():
            writer.closeStream()
