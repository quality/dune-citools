import argparse
import os
import yaml

# The StringIO library moved in python3
try:
    from StringIO import StringIO
except ImportError:
    from io import StringIO

# Define defaults for all the configuration values
_default_config = {
    'sources_path': os.path.expanduser('~/sources'),
    'build_path': os.path.expanduser('~/build'),
}

# Define storage for memoization of the parsed configuration
_memoized_config = {}


def get_argument(key, default=None):
    # Find environment variables prefixed with DUNECI_
    env_args = {k[7:].lower(): os.environ[k] for k in os.environ if k.startswith('DUNECI_')}

    # Parse command line arguments
    parser = argparse.ArgumentParser()
    parser.add_argument('-s', '--sources-path')
    parser.add_argument('-b', '--build-path')
    cmdline_args = {k: v for k, v in vars(parser.parse_args()).items() if v is not None}

    return cmdline_args.get(key, env_args.get(key, _default_config.get(key, default)))


def get_duneci_option(key, default=None, path=os.environ.get('CI_PROJECT_DIR', '.')):
    global _memoized_config
    dunecifile = os.path.abspath(os.path.join(path, '.dune-ci.yml'))
    if dunecifile not in _memoized_config and os.path.exists(dunecifile):
        with open(dunecifile, 'r') as f:
            _memoized_config[dunecifile] = yaml.safe_load(StringIO('\n'.join([os.path.expandvars(l) for l in f.readlines()])))
        # An empty .dune-ci.yml results in None being parsed
        if _memoized_config[dunecifile] is None:
            _memoized_config[dunecifile] = {}
    return _memoized_config.get(dunecifile, {}).get(key, default)
