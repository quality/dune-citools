""" A data structure for a dune.module file """

import pyparsing as pp
import os

from dune.citools.error import DuneCIError


class DuneModuleFile(object):

    def __init__(self, module=None, version=None, maintainer=None, whitespace_hook=False, depends=[], suggests=[]):
        assert(module)
        self.module = module
        self.version = version
        self.maintainer = maintainer
        self.whitespace_hook = whitespace_hook
        self.depends = depends
        self.suggests = suggests

    def __str__(self):
        return "Dune Module file for module {}".format(self.module)


class DuneModuleFileParser(object):
    _debug = False

    singleKeys = ["Module", "Version", "Maintainer", "Whitespace-Hook"]
    multiKeys = ["Depends", "Suggests"]

    def __init__(self):
        self._parser = self.construct_bnf()
        if DuneModuleFileParser._debug:
            print("The BNF: {}".format(self._parser))

    def _bnf_from_key(self, key):
        bnf = pp.Literal('{}:'.format(key)).suppress() + pp.Word(pp.printables).setParseAction(self.setVariable(key.replace('-', '_').lower()))
        if DuneModuleFileParser._debug:
            print("Constructing a BNF for {}: {}".format(key, bnf))
        return bnf

    def _bnf_versioned_list(self, key):
        requirement = pp.Word(pp.printables, excludeChars="()").setParseAction(self.appendVariable(key.lower()))
        version = pp.Optional(pp.Literal("(").suppress() + pp.Word(pp.printables + " ", excludeChars=")") + pp.Literal(")").suppress())
        # TODO do not suppress the version here, but use it instead
        entry = requirement + version.suppress()
        requlist = pp.ZeroOrMore(entry)
        bnf = pp.Literal('{}:'.format(key)).suppress() + requlist
        return bnf

    def construct_bnf(self):
        simplekeys = [self._bnf_from_key(k) for k in DuneModuleFileParser.singleKeys]
        multikeys = [self._bnf_versioned_list(k) for k in DuneModuleFileParser.multiKeys]
        comment = [(pp.Literal('#') + pp.Word(pp.printables + " ")).suppress(), pp.Empty()]
        bnflist = simplekeys + multikeys + comment

        line = bnflist[0]
        for b in bnflist[1:]:
            line = line | b
        line = line + pp.LineEnd()

        return line

    def appendVariable(self, var):
        def _parseAction(origString, loc, tokens):
            if DuneModuleFileParser._debug:
                print("Appending to self.result[{}] to {}".format(var, tokens[0]))
            if var not in self.result:
                self.result[var] = []
            self.result[var].append(tokens[0])
        return _parseAction

    def setVariable(self, var):
        def _parseAction(origString, loc, tokens):
            if DuneModuleFileParser._debug:
                print("Setting self.result[{}] to {}".format(var, tokens[0]))
            self.result[var] = tokens[0]
        return _parseAction

    def apply(self, filename):
        if DuneModuleFileParser._debug:
            print("Parsing file: {}".format(filename))
        with open(filename, 'r') as f:
            self.result = {}
            for line in f:
                if DuneModuleFileParser._debug:
                    print("Parsing line: {}".format(line[:-1]))
                self._parser.parseString(line)

        return DuneModuleFile(**self.result)


def parse_dune_module_file(filename):
    if not os.path.exists(filename):
        raise DuneCIError("No dune.module file found at {}".format(os.path.abspath(filename)))
    return DuneModuleFileParser().apply(filename)
