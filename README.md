# How does the Dune CI system work?

The Dune CI system heavily relies on GitLab CI. To get going
with continuous integration for your Dune project, do the following
* If not already, version-control your project with [git](https://git-scm.com/) and
  host it on a GitLab server. The [main Dune GitLab](https://gitlab.dune-project.org)
  is available to all Dune users.
* Add a `.gitlab-ci.yml` to the root directory of your project.
  This file configures the builds in the CI system as explained
  in [GitLab's documentation](http://docs.gitlab.com/ce/ci/yaml/README.html).
  TODO: point to easy Dune-style example.
* Read through the rest of this documentation to find out, what
  fancy stuff you can do to get the best testing experience for
  your code.

# What happens during a build?

GitLab CI allows to define so-called *stages* of a build in `.gitlab-ci.yml`.
We recommend using this concept to split the build process into several parts.
This allows to grasp quickly, which part of the build process failed. The
usual stages needed for a Dune application are:
* [`update`: Preparing the sources](#update)
* [`configure`: Run cmake to configure the build](#configure)
* [`build`: Build the Dune libraries](#build)
* [`build_tests`: Build the test executables](#build_tests)
* [`test`: Run the test suite](#test)

`dune-citools` provides you with tools for all these stages. These
tools aim at making writing your `.gitlab-ci.yml` files as easy as
possible while adding even more nice features. All these tools are
configured through a second, dune-specific configuration file called
`dune-ci.yml` (The [yaml format](http://www.yaml.org/) is a format for
configuration files that has some nice features compared to ini files,
such as lists).

<a name="update"></a>
# The update stage - Preparing the sources

The update stage performs the following steps:
* Make sure all needed Dune modules are cloned
* Pull latest changes from all needed Dune modules
* Switch all required Dune modules to suitable branches

To run this stage, simply call it as part of your jobs `script` in
`.gitlab-ci.yml`:

```
your-job:
  stage: update
  script:
    - duneci_update
```

In the following, the semantics of above steps, as well as configuration
options will be explained.

## Cloning the needed sources

Gitlab CI will provide a clone of the repository, that the current build
was triggered on (in the following: *the current module*), in
the working directory. During the update stage, `duneci_update` will
place all other needed modules into *sources path*.

The *sources path* is determined by the first applying of these options:
* Read command line option `--sources-path=<path>` to `duneci_update`
* Read environment variable `DUNECI_SOURCES_PATH`
* Use standard path `~/sources`

The `dune.module` file of the current module file is parsed to determine
Dune module dependencies. For each needed Dune module, the first succesful
of these strategies is applied:
* Check `.dune-ci.yml` of the current module for a manually specified
  upstream URL. The following example shows how to set a custom remote for dune-foo:

```
upstream:
  dune-foo:
    url: https://gitlab.mysite.com/dune-foo.git
```

* Check the `.dune-ci.yml` of required modules of the current modules for
  any manually specified upstream URLs. If multiple, contradicting URLs are
  found, there is no guarantee about which one will be picked.
* Use the *official* repository for the module from a database. By *official*
  we mean the repository, that the maintainers of the respective module
  advertise as the main repository.
* Throw an error.

Note, that if a remote repository is non-public, the runner needs to have
an authentication method. There are several ways to achieve this, but we
only support and describe one here:
* Go to the project settings of the non-public dependency of the current
  module and find the *Runners token* under CI/CD Pipelines.
* Go to the project settings of the current project and add the above
  access token as a secret variable.
* Adjust your `.dune-ci.yml` to list the token:

```
upstream:
  dune-foo:
    url: https://gitlab.mysite.com/dune-foo.git
    token: $DUNE_FOO_CI_ACCESS_TOKEN
```

**IMPORTANT NOTICE**: Using this method, all users that have at least
developer access on the current project can steal your access token by
modifying `.gitlab-ci.yml` to print the secret environment variable.

## Switch the needed sources to a suitable branch

The correct branch of the current module is already checked out by Gitlab CI.
For all other modules, the first of the following strategies that results in
an existing branch is used:
* Check `.dune-ci.yml` of the current module for a manually specified branch.
  The following example shows an example `.dune-ci.yml`, that selects a
  custom branch for the requirement dune-foo:

```
upstream:
  dune-foo:
    branch: feature/my-super-feature
```

* Check the `.dune-ci.yml` of required modules of the current modules for
  any manually specified branch. If multiple, contradicting branches are
  found in this way, there is no guarantee about which one will be picked.
* Determine the closest merge-base of the branch on the current module
  and use it on other modules. If multiple candidates are available, use
  them in the following order:
  * master
  * releases/*
  * any other
* Use master branch


<a name="configure></a>
# The configure stage - Running CMake

<a name="build"></a>
# The build stage - Building Dune module libraries

<a name="build_tests"></a>
# The build_tests stage - Building test executables

<a name="test"></a>
# The test stage - Running the test suite

# Acknowledgments

The work by Timo Koch and Dominic Kempf is supported by the
ministry of science, research and arts of the federal state of
Baden-Württemberg (Ministerium für Wissenschaft, Forschung
und Kunst Baden-Württemberg).
