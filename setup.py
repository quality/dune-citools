import sys

from setuptools import setup
from setuptools.command.test import test as TestCommand


class PyTest(TestCommand):

    def initialize_options(self):
        TestCommand.initialize_options(self)

    def finalize_options(self):
        TestCommand.finalize_options(self)
        self.test_args = []
        self.test_suite = True

    def run_tests(self):
        import pytest
        err = pytest.main(['--pep8'])
        sys.exit(err)


setup(
    name='dune.citools',
    namespace_packages=['dune'],
    version='2.4',
    description='Python tools for running Dune in CI environments',
    url='http://www.dune-project.org',
    author='Dominic Kempf',
    author_email='dominic.kempf@iwr.uni-heidelberg.de',
    license='BSD',
    install_requires=[
        'gitpython',
        'pyaml',
        'pyparsing',
        'mock',
        'pytest',
        'pytest-pep8',
    ],
    packages=[
        'dune.citools',
        'dune.citools.logging',
        'dune.citools.stages',
    ],
    entry_points={
        'console_scripts': [
            'duneci_update=dune.citools.stages.update:dune_citools_update',
            'duneci_cmake=dune.citools.stages.dunecontrol:_cmake',
            'duneci_build=dune.citools.stages.dunecontrol:_build',
            'duneci_build_tests=dune.citools.stages.dunecontrol:_build_tests',
            'duneci_test=dune.citools.stages.dunecontrol:_ctest',
        ],
    },
    cmdclass={'test': PyTest}
)
