""" Unit tests for the updating step

The tests are automatically set up from the file yaml files in the test directory
starting with update_ . To add a new unit test, create a (publicly visible) repository
that should be tested. Then, add a yaml file specifying the expected result:

url: <remote-url-to-be-tested>
<branch1-to-test>:
  dune-common:
    url: <expected_url>
    branch: <expected_branch>
  dune-foo:
    url: <expected_url> | "default"
    branch: <expected_branch>
  ...
<branch2-to-test>:
  ...

Here, url and branch of the needed modules can be omitted and will default to
standard Dune repositories resp. master.
"""

import git
import mock
import os
import pytest
import re
import shutil
import sys
import yaml

from dune.citools.stages.update import dune_citools_update
from dune.citools.config import get_argument


def checkout_branch(repo, branch):
    # Checkout the branch
    for ref in repo.refs:
        if ref.name == 'origin/{}'.format(branch):
            ref = git.Head.create(repo, branch, reference=ref)
            ref.checkout()


@pytest.fixture
def setup(request, tmpdir, branch, testname, testdata):
    # Clone the repository
    repo_path = tmpdir.join('current_source')
    url = testdata[testname]["url"]
    repo = git.Repo.clone_from(url, str(repo_path))

    # Store old cwd and switch to new one
    old_cwd = os.getcwd()

    def change_back_cwd():
        os.chdir(old_cwd)

    request.addfinalizer(change_back_cwd)

    os.chdir(str(repo_path))

    # Checkout the branch
    checkout_branch(repo, branch)

    return repo_path


def test_update(setup, testname, branch, testdata, tmpdir):
    # This might be a non-existent test!
    if branch not in testdata[testname]:
        return

    # Extract data from the testdata
    url = testdata[testname]["url"]

    print("Testing repository: {}".format(url))
    print("Testing branch: {}".format(branch))

    with mock.patch.object(sys, 'argv', ['duneci_update', '-s', str(setup.join('..', 'source'))]), mock.patch.dict(os.environ, {"CI_BUILD_REF_NAME": branch, "CI_PROJECT_DIR": str(setup)}):
        # Run the actual script
        dune_citools_update()

        # Inspect the result on the filesystem and compare to the specified test output
        for module in testdata[testname][branch]:
            # Get the git object representing the clone of this module:
            repo = git.Repo(os.path.join(get_argument("sources_path"), module))

            # Compare the active branch to the expected one
            if "branch" in testdata[testname][branch][module]:
                expected_branch = testdata[testname][branch][module]["branch"]
                assert(repo.active_branch.name == expected_branch)

            # Check that the head is the same as on the specified remote
            if "url" in testdata[testname][branch][module]:
                expected_url = testdata[testname][branch][module]["url"]
                branch = repo.active_branch.name

                reference_path = tmpdir.join('reference_source', module)
                if os.path.exists(str(reference_path)):
                    shutil.rmtree(str(reference_path))
                ref_repo = git.Repo.clone_from(expected_url, str(reference_path))
                checkout_branch(ref_repo, branch)
                assert(ref_repo.head.commit.hexsha == repo.head.commit.hexsha)


def pytest_generate_tests(metafunc):
    configs = {}

    # Glob for yaml files starting with update_:
    for _, _, filedir in os.walk(os.path.join(os.getcwd(), 'test')):
        for fn in filedir:
            match = re.match('update_(.*).yml', fn)
            if match:
                # Get the name from the filename
                testname = match.group(1)

                # Read the test specification!
                config = yaml.safe_load(open(os.path.join(os.getcwd(), 'test', fn)))

                # Store it in the configs dict
                configs[testname] = config

    # Gather a list of specified branches
    branches = set().union(*[set(configs[test].keys()) for test in configs]) - set(["url"])

    # Define variability
    metafunc.parametrize("testname", configs.keys())
    metafunc.parametrize("branch", branches)
    metafunc.parametrize("testdata", [configs])
