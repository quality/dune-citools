import os

from dune.citools.logging import log_dir, get_log_dir


def test_logdir():
    wd = os.getcwd()
    assert(get_log_dir() == wd)
    with log_dir('bla'):
        assert(get_log_dir() == os.path.join(wd, 'bla'))
    with log_dir('/home/foo'):
        with log_dir('bla'):
            assert(get_log_dir() == "/home/foo/bla")
            with log_dir('bar'):
                assert(get_log_dir() == "/home/foo/bla/bar")
            with log_dir('/home/bar'):
                assert(get_log_dir() == "/home/bar")
            assert(get_log_dir() == "/home/foo/bla")
