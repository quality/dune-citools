#!/bin/bash

# Build the Docker images for CI testing of dune-citools
# Only needed by myself!

set -e

# Build the images
docker build --tag=dkempf/citools-image:python2 --file=python2.Dockerfile .
docker build --tag=dkempf/citools-image:python3 --file=python3.Dockerfile .

# Push the images
docker push dkempf/citools-image:python2
docker push dkempf/citools-image:python3

# Remove the images locally
docker rmi dkempf/citools-image:python2
docker rmi dkempf/citools-image:python3
